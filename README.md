# xnconvert

A powerful batch image-converter and resizer

https://www.xnview.com/en/xnconvert

## Getting started

<br><br>
How to clone this repository:

```
git clone https://gitlab.com/azul4/content/multimedia/xnconvert.git
```
